import { localStore } from "./localStore";

export enum SignFont {
    Luciole = "Luciole"
}

export class ThemeSettings {
    background_image: string;
    background_blur: boolean;
    skip_splashscreen: boolean;
    sign: {
        text: string;
        font: SignFont;
        flicker: boolean;
        show_clock: boolean;
    }
    colors: {
        accent_color: string;
    };
}

const initialSettings: ThemeSettings = {
    background_image: null,
    background_blur: false,
    skip_splashscreen: true,
    sign: {
        text: "Bienvenue!",
        font: SignFont.Luciole,
        flicker: false,
        show_clock: false,
    },
    colors: {
        accent_color: '#262e42'
    }
}

export const themeSettings = localStore('settings', initialSettings);
